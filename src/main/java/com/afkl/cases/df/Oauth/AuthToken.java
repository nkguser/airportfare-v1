package com.afkl.cases.df.Oauth;

import java.io.IOException;

import org.springframework.stereotype.Service;

@Service
public interface AuthToken {

	String getToken() throws IOException;
}
