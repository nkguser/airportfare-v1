package com.afkl.cases.df.Oauth;

import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class TokenAPI implements AuthToken {
	
	@Autowired
	private RestTemplate restTemplate;
	
	private static Logger logger = LoggerFactory.getLogger(TokenAPI.class);

    @Value("${simple-travel-api.auth.username}")
    private String username;

    @Value("${simple-travel-api.auth.password}")
    private String password;

    @Value("${simple-travel-api.url}")
    private String simpleTravelApiUrl;

    private volatile String token;
    private volatile long tokenExpiresAtMillis = Long.MIN_VALUE;
	
	
	public String generateToken() throws IOException {
		
		ResponseEntity<String> response = null;

		String access_token_url = simpleTravelApiUrl+"/oauth/token";
		
		String credentials = username+":"+password;
		String encodedCredentials = new String(Base64.getEncoder().encodeToString(credentials.getBytes()));

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("content-type", "application/x-www-form-urlencoded");
		headers.add("Authorization", "Basic " + encodedCredentials);

		HttpEntity<String> request = new HttpEntity<>(headers);

		access_token_url += "?grant_type=client_credentials";

		response = restTemplate.exchange(access_token_url, HttpMethod.POST, request, String.class);
		
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = null;
		try {
			node = mapper.readTree(response.getBody());
		} catch (Exception e) {
			e.printStackTrace();
		}
		token = node.path("access_token").asText();
		logger.info(token);
		
		tokenExpiresAtMillis = System.currentTimeMillis() + node.path("expires_in").asInt() * 1000;
		
		return token;
	}
	
    @Override
    public String getToken() throws IOException {
        if (System.currentTimeMillis() > tokenExpiresAtMillis) {
            synchronized (this) {
                if (System.currentTimeMillis() > tokenExpiresAtMillis) {
                    generateToken();
                }
            }
        }
        return token;
    }
}
