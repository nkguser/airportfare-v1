package com.afkl.cases.df.Exception;

import org.springframework.http.HttpStatus;

public class TokenException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4233715598294666508L;
	/**
	 * 
	 */
	
	private final HttpStatus status;

	public TokenException(String exception, HttpStatus status) {

		super(exception);
		this.status = status;
	}

	public HttpStatus getStatus() {
		return this.status;
	}
}
