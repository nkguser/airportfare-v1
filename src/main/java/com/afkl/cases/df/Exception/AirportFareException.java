package com.afkl.cases.df.Exception;

import org.springframework.http.HttpStatus;

public class AirportFareException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7782304234057328861L;
	
	private final HttpStatus status;

	public AirportFareException(String exception, HttpStatus status) {

		super(exception);
		this.status = status;
	}

	public HttpStatus getStatus() {
		return this.status;
	}
}
