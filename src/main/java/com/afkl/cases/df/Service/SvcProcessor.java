package com.afkl.cases.df.Service;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.afkl.cases.df.Model.Airports;

@Service
public interface SvcProcessor {

    public HttpEntity<String> appendToken() throws IOException;
    public ResponseEntity<List<Airports>> findAirportLocation(String url) throws IOException;
    public CompletableFuture<ResponseEntity<String>> fare(String url) throws IOException;
    public CompletableFuture<ResponseEntity<String>> origin(String url) throws IOException;
    public CompletableFuture<ResponseEntity<String>> destination(String url) throws IOException;
}
