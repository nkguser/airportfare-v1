package com.afkl.cases.df.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.afkl.cases.df.Exception.AirportFareException;
import com.afkl.cases.df.Exception.TokenException;
import com.afkl.cases.df.Model.Airports;
import com.afkl.cases.df.Model.Locations;
import com.afkl.cases.df.Model.Locations.Location;
import com.afkl.cases.df.Oauth.AuthToken;
import com.afkl.cases.df.Utils.AirportUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class SvcProcessorAPI implements SvcProcessor {
	
	@Autowired
	private AuthToken authToken;
	
	@Autowired
    private RestTemplate restTemplateLocations,
                         restTemplateFare,
                         restTemplateOrigin,
                         restTemplateDest;

	public HttpEntity<String> appendToken() throws IOException {
		
		HttpHeaders headers = new HttpHeaders();
        
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));     
		
		try {
			headers.add("Authorization", "Bearer " + authToken.getToken());     
			return new HttpEntity<>(headers);
		} catch(Exception e) {
			
			throw new TokenException(e.getMessage(), HttpStatus.NOT_FOUND);
		}
    }
	
    public ResponseEntity<List<Airports>> findAirportLocation(String url) throws IOException {
		
    	HttpEntity<String> request = appendToken();  
    	
    	try {
    		
	    	ResponseEntity<String> airportLocations  = restTemplateLocations.exchange(url,HttpMethod.GET,request, String.class);
	    	
	        JSONObject root = new JSONObject(airportLocations.getBody());
	        JSONObject root_level1 = root.getJSONObject("_embedded");
	         
	        ObjectMapper objectMapper = new ObjectMapper();
	
	        //convert json string to object
	        Locations locations = objectMapper.readValue(root_level1.toString(), Locations.class);
	
	        List<Airports> airports = new ArrayList<>();
	        for (final Location location : locations.getLocations()) {
	             AirportUtil airportUtil = new AirportUtil();
	             airportUtil.getLocations(location, airports);
	         }
	    	
			return new ResponseEntity<>(airports,HttpStatus.OK);
			
    	}catch(Exception e){
    		
    			throw new AirportFareException(e.getMessage(), HttpStatus.NOT_FOUND);
    			
    	}
    }
	
	@Async
	public CompletableFuture<ResponseEntity<String>> fare(String url) throws IOException{

	  HttpEntity<String> request = appendToken();
	  ResponseEntity<String> futureFare  = restTemplateFare.exchange(url,HttpMethod.GET,request, String.class);
	  return CompletableFuture.completedFuture(futureFare);
	}
	
	@Async
	public CompletableFuture<ResponseEntity<String>> origin(String url) throws IOException{

	  HttpEntity<String> request = appendToken();
	  ResponseEntity<String> futureOrigin  = restTemplateOrigin.exchange(url,HttpMethod.GET,request, String.class);
	  return CompletableFuture.completedFuture(futureOrigin);
	}

	@Async
	public CompletableFuture<ResponseEntity<String>> destination(String url) throws IOException{

	  HttpEntity<String> request = appendToken();
	  ResponseEntity<String> futureDest  = restTemplateDest.exchange(url,HttpMethod.GET, request, String.class);
	  return CompletableFuture.completedFuture(futureDest);
	}
 
}
