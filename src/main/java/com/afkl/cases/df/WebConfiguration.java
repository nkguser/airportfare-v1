package com.afkl.cases.df;

import java.util.concurrent.Executor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.afkl.cases.df.Info.StatisticsInterceptor;

@Configuration
@EnableAsync
@EnableWebMvc
public class WebConfiguration implements WebMvcConfigurer{
	
	
	@Value("${simple-travel-ui.url}")
	private String simpleTravelUiUrl;
	
	@Bean
	@Scope("prototype")
	public RestTemplate rt() {
		
		return new RestTemplate();
	}
	
	@Bean
	public Executor taskExecutor() {
	    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
	    executor.setCorePoolSize(2);
	    executor.setMaxPoolSize(2);
	    executor.setQueueCapacity(500);
	    executor.setThreadNamePrefix("async-task--");
	    executor.initialize();
	    return executor;
    }
	
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("/*.js").addResourceLocations("classpath:/static/");
		registry.addResourceHandler("/*.css").addResourceLocations("classpath:/static/");
		registry.addResourceHandler("/*.jpg").addResourceLocations("classpath:/static/");
    }

    @Bean
    public StatisticsInterceptor createStatisticsInterceptor() {
        return new StatisticsInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(createStatisticsInterceptor());
    }
}
