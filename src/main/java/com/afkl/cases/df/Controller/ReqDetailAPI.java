package com.afkl.cases.df.Controller;

import java.util.concurrent.Callable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.afkl.cases.df.Info.StatisticReport;
import com.afkl.cases.df.Info.StatisticsBean;


@RestController
public class ReqDetailAPI {

    private StatisticsBean statisticBean;

    @Autowired
    public void StatisticsController(StatisticsBean statisticBean) {
        this.statisticBean = statisticBean;
    }

    @GetMapping(value = "/RequestInfo", produces = "application/json")
    public Callable<StatisticReport> stat() {
        return () -> statisticBean.getStatisticReport();
    }
}
