package com.afkl.cases.df.Controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.afkl.cases.df.Exception.AirportFareException;
import com.afkl.cases.df.Exception.TokenException;
import com.afkl.cases.df.Model.CustomErrorResponse;

@ControllerAdvice
public class CustomErrorHandler extends ResponseEntityExceptionHandler {

	  @ExceptionHandler({AirportFareException.class, TokenException.class})
	  public ResponseEntity<CustomErrorResponse> airportNotFound(AirportFareException ex, WebRequest request) {

	        CustomErrorResponse errors = new CustomErrorResponse();;
	        errors.setError (ex.getMessage());
	        errors.setStatus(ex.getStatus());

	        return new ResponseEntity<>(errors, ex.getStatus());
	  }	    
}
