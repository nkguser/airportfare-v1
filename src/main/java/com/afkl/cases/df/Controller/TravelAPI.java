package com.afkl.cases.df.Controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.json.JSONObject;

import com.afkl.cases.df.Model.Airports;
import com.afkl.cases.df.Model.Fare;
import com.afkl.cases.df.Service.SvcProcessor;
import com.afkl.cases.df.Exception.AirportFareException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@RestController
public class TravelAPI {

	private static Logger logger = LoggerFactory.getLogger(TravelAPI.class);
		
	@Autowired
	private SvcProcessor svcProcessor;
	
    @Value("${simple-travel-api.url}")
    private String simpleTravelApiUrl;
    
    
    public TravelAPI(SvcProcessor svcProcessor) {
        this.svcProcessor = svcProcessor;
    }

    @GetMapping(value = "/airports", produces = "application/json")
    public ResponseEntity<List<Airports>> query() throws IOException   {

         String urlOrigin = simpleTravelApiUrl + "/airports";
         return svcProcessor.findAirportLocation(urlOrigin);
    }
    
	@GetMapping(value = "/airport", produces = "application/json")
	public ResponseEntity<List<Airports>> pagedquery(@RequestParam(name = "lang") String lang,
			                                         @RequestParam(name = "page") String page,
			                                         @RequestParam(name = "size") String size) throws IOException   {
		String urlAirport = simpleTravelApiUrl + "/airports?lang=" + lang +"&page=" + page + "&size=" + size;
		logger.info("urlAirport:{}", urlAirport);
		return svcProcessor.findAirportLocation(urlAirport);
	}
    
    @GetMapping(value = "/fares/{origin}/{destination}", produces = "application/json")
    public Callable<List<Fare>> calculateFare(@PathVariable("origin") String origin,
                                              @PathVariable("destination") String destination) throws InterruptedException, ExecutionException, IOException {
        
    	String urlFare = simpleTravelApiUrl + "/fares/" + origin + "/" + destination;
    	String urlOrigin = simpleTravelApiUrl + "/airports/" + origin;
    	String urlDest = simpleTravelApiUrl + "/airports/" + destination;
    	
    	try {
	    	CompletableFuture<ResponseEntity<String>> fareResponse = svcProcessor.fare(urlFare);
	    	CompletableFuture<ResponseEntity<String>> originResponse = svcProcessor.origin(urlOrigin);
	    	CompletableFuture<ResponseEntity<String>> destResponse = svcProcessor.destination(urlDest);
	
	        //Wait until all are done...!!!
	        CompletableFuture.allOf(fareResponse, originResponse, destResponse).join();
	      
	        JSONObject fare = new JSONObject(fareResponse.get().getBody());
	        JSONObject ori  = new JSONObject(originResponse.get().getBody());
	        JSONObject dest = new JSONObject(destResponse.get().getBody());
	       
	        logger.info("Success");
            List<Fare> list =  new ArrayList<>();
	        list.add(new Fare(fare.getDouble("amount"),ori.getString("code"),dest.getString("code")));
	        
	        return () -> list;
    	}catch(Exception e) {
    		throw new AirportFareException(e.getMessage(), HttpStatus.NOT_FOUND);
    	}
        
    }

}
