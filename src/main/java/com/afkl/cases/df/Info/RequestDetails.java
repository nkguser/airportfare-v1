package com.afkl.cases.df.Info;

public class RequestDetails {

    private final long executionTime;
    private final int httpCode;

    public RequestDetails(long executionTime, int httpCode) {
        this.executionTime = executionTime;
        this.httpCode = httpCode;
    }

    public long getExecutionTime() {
        return executionTime;
    }

    public int getHttpCode() {
        return httpCode;
    }
}
