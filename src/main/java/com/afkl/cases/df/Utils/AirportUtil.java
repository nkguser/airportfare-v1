package com.afkl.cases.df.Utils;

import java.util.List;

import com.afkl.cases.df.Model.Airports;
import com.afkl.cases.df.Model.Locations.Location;

public class AirportUtil {

    public static void getLocations(Location location, List<Airports> airports) {
            if(location!= null) {
                Airports airport = new Airports();

                airport.setName(location.getName());
                airport.setCode(location.getCode());
                airport.setDescription(location.getDescription());

                airports.add(airport);

                AirportUtil.getLocations(location.getParent(), airports);
            }        
    }
}
