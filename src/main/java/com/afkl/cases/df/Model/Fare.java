package com.afkl.cases.df.Model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Fare {

    double amount;
    String source;
    String destination;
}
