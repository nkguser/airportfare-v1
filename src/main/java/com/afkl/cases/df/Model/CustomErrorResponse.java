package com.afkl.cases.df.Model;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomErrorResponse {
	
    private HttpStatus  status;
    private String  error; 
}
