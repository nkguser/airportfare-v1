package com.afkl.cases.df.Model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Coordinates {

    private double latitude, longitude;
}
