package com.afkl.cases.df.Model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Airports {

    String code;
    String name;
    String description;        
}
