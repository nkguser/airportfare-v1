
1. Open the terminal window.
2. Check out the code base to your directory with the following below commands:
		git clone -b master https://bitbucket.org/nkguser/airportfare-v1/src/master/
3. Go to the direcoty -> cd master
3. Run the command - "gradlew bootRun"
4. Open another Terminal Window and run the mock service - "gradlew bootRun"
4. Wait for both the application to deploy on localhost port number : 9000 and 8080
5. Make sure both the application should be UP.
5. Run the Web Application using URL - http://localhost:9000/travel/index.html and wait for the application to get ready.
6. Some sample input to check Fares between Source and Destionation as : 

   Source - 'YOW'
   Destination - 'JRO'
   
   HIT 'SEARCH' Button
   
7. Click the "Airport List" hyperlink present on the webpage to find list of airport codes.
8. Click the "HTTP Request Details" hyperlink to check the website statistics.
