import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { SearchFareComponent } from './search-fare/search-fare.component';
import { HttpInfoComponent } from './http-info/http-info.component';
import { AirportListComponent } from './airport-list/airport-list.component';

const routes: Routes = [
    { path: '', component: SearchFareComponent },
    {path: 'httpinfo', component: HttpInfoComponent},
    {path: 'AirportList', component: AirportListComponent},

];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {useHash: true})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingcomponents = [HttpInfoComponent, AirportListComponent];
