import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { SearchFareService } from '../shared/Service/search-fare.service';
import { IAirportModel } from '../shared/model/Airport.model';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-airport-list',
  templateUrl: './airport-list.component.html',
  styleUrls: ['./airport-list.component.scss']
})
export class AirportListComponent implements OnInit {

  private getAirports = null;
  public airPortDetails;

  dataSource: MatTableDataSource<IAirportModel>;

  displayedColumns = [
    'Airport Name', 'Airport Code', 'Airport Description'
  ];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private searchFareService: SearchFareService) { }

  /**
   * on component initalization
   */
  ngOnInit() {
    this.initGetAirportDetails();
    this.getAirports.get();
  }

  initGetAirportDetails() {
    this.getAirports = this.searchFareService.getAirportDetails();
    this.getAirports.response.subscribe((res: IAirportModel) => {
      if (res) {
        this.airPortDetails = res;
        this.dataSource = new MatTableDataSource(this.airPortDetails);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        console.log(this.airPortDetails);
      }
    });
  }
}
