import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule, routingcomponents } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SearchFareComponent } from './search-fare/search-fare.component';
import { HttpClientModule } from '@angular/common/http';
import { SearchFareService } from './shared/Service/search-fare.service';
import { SearchDropdownComponent } from './shared/component/search-dropdown/search-dropdown.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTableModule, MatPaginatorModule, MatSortModule} from '@angular/material';
import { HttpInfoService } from './shared/Service/http-info.service';
import { HttpInfoComponent } from './http-info/http-info.component';
import { AirportListComponent } from './airport-list/airport-list.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchFareComponent,
    SearchDropdownComponent,
    routingcomponents,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule
  ],
  providers: [SearchFareService, HttpInfoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
