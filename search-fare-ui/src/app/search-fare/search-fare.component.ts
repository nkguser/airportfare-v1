import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { SearchFareService } from '../shared/Service/search-fare.service';
import { IAirportModel } from '../shared/model/Airport.model';
import { IFareModel } from '../shared/model/Fare.model';
import { FormGroup, FormControl } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-search-fare',
  templateUrl: './search-fare.component.html',
  styleUrls: ['./search-fare.component.scss']
})
export class SearchFareComponent implements OnInit {
  private getAirports = null;
  private getFareDetail = null;
  public airPortDetails;
  public fareDetails;
  sourceAirport: Array<IAirportModel> = null;
  destinationAirport: Array<IAirportModel> = null;
  dataSource: MatTableDataSource<IFareModel>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns = [
    'source', 'destination', 'amount'
  ];

  public searchForm = new FormGroup({
    sourceAirport: new FormControl(''),
    sourceAirportCode: new FormControl(''),
    destinationAirport: new FormControl(''),
    destinationAirportCode: new FormControl('')
  });

  constructor(private searchFareService: SearchFareService) { }

  /**
   * on component initalization
   */
  ngOnInit() {
    this.init();
    this.getAirports.get();
  }

  init() {
    this.initGetAirportDetails();
    this.initGetFareDetails();
  }

  initGetAirportDetails() {
    this.getAirports = this.searchFareService.getAirportDetails();
    this.getAirports.response.subscribe((res: IAirportModel) => {
      if (res) {
        this.airPortDetails = res;
      }
    });
  }

  initGetFareDetails() {
    this.getFareDetail = this.searchFareService.getFareDetails();
    this.getFareDetail.response.subscribe((res) => {
      if (res) {
        this.fareDetails = res;
        this.dataSource = new MatTableDataSource(this.fareDetails);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }

  submitSearch() {
    if (this.searchForm.valid) {
      const param = {
        origin: this.searchForm.value.sourceAirportCode,
        destination: this.searchForm.value.destinationAirportCode
      };
      this.getFareDetail.get(param);
    }
  }

  addSourceSelection(selectedItem: IAirportModel) {
    this.searchForm.patchValue({
      sourceAirport: selectedItem.description,
      sourceAirportCode: selectedItem.code
    });
    this.sourceAirport = null;
  }

  addDestinationSelection(selectedItem: IAirportModel) {
    this.searchForm.patchValue({
      destinationAirport: selectedItem.description,
      destinationAirportCode: selectedItem.code
    });
    this.destinationAirport = null;
  }


  public changeSourceAirport(event: any) {
    if (event.target.value.length > 2) {
      this.sourceAirport = this.filterAirport(event.target.value);
    } else if (event.target.value.length === 0) {
      this.sourceAirport = null;
    }
  }

  public changeDestinationAirport(event: any) {
    if (event.target.value.length > 2) {
      this.destinationAirport = this.filterAirport(event.target.value);
    } else if (event.target.value.length === 0) {
      this.destinationAirport = null;
    }
  }

  private filterAirport(keyword: string): Array<IAirportModel> {
    let searchItemByCode = [];
    let searchItemByName = [];
    let searchItemByDesc = [];

    let result = [];
    /// filter by code
    searchItemByCode = this.airPortDetails ? this.airPortDetails.filter(item => item.code.search(new RegExp(keyword, 'i')) > - 1)
                                           : [];

    /// filter by name
    searchItemByName = this.airPortDetails ? this.airPortDetails.filter(item => item.name.search(new RegExp(keyword, 'i')) > - 1)
                                           : [];

    /// filter by description
    searchItemByDesc = this.airPortDetails ? this.airPortDetails.filter(item => item.description.search(new RegExp(keyword, 'i')) > -1)
                                           : [];

    result = searchItemByCode.concat(searchItemByName);
    result = result.concat(searchItemByDesc);
    result = Array.from(new Set(result.map(s => s.code)))
      .map(code => {
        return {
          code,
          name: result.find(s => s.code === code).name,
          description: result.find(s => s.code === code).description
        };
      });
    return result;
  }

}
