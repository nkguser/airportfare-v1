import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

@Injectable()
export class SearchFareService {
    /**
     *
     */
    constructor(private httpClient: HttpClient) {

    }

    getAirportDetails() {
        const airPortDetails = new Subject();
        const response = airPortDetails.pipe(
            switchMap(() => {
                return this.httpClient.get('/travel/airports')
                                      .pipe(catchError(err => { return 'Airport Search Service Broke';}));
            })
        );
        return {
            get: () => airPortDetails.next(),
            response
        };

    }

    getFareDetails() {
        const searchDeals = new Subject();
        const header = { };
        const response = searchDeals.pipe(
            switchMap((param: any) => {
                return this.httpClient.get('/travel/fares/' + param.origin + '/' + param.destination)
                                      .pipe(catchError(err => { return 'Fare Search Service Broke';}));
            })
        );
        return {
            get: (param: any) => searchDeals.next(param),
            response
        };
    }
}
