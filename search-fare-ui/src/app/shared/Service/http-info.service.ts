import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

@Injectable()
export class HttpInfoService {
    /**
     *
     */
    constructor(private httpClient: HttpClient) {

    }

    getHttpDetails() {
        const httpDetails = new Subject();
        const response = httpDetails.pipe(
            switchMap(() => {
                return this.httpClient.get('/travel/RequestInfo')
                                      .pipe(catchError(err => { return 'HTTP Stat Service Broke';}));
            })
        );
        return {
            get: () => httpDetails.next(),
            response
        };

    }
}
