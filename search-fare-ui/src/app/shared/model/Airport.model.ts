export interface IAirportModel {
    code: string;
    name: string;
    description: string;
}
