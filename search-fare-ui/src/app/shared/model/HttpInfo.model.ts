export interface IHttpInfoModel {
    requestCount: string;
    request200Count: string;
    request4XXCount: string;
    request5XXCount: string;
    totalResponseTimeMillis: string;
    avgResponseTimeMillis: string;
    minResponseTimeMillis: string;
    maxResponseTimeMillis: string;
}
