export interface IFareModel {
    amount: string;
    origin: string;
    destination: string;
}