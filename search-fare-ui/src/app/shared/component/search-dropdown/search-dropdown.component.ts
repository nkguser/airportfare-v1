import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search-dropdown',
  templateUrl: './search-dropdown.component.html',
  styleUrls: ['./search-dropdown.component.scss']
})
export class SearchDropdownComponent implements OnInit {

  @Input() model: Array<any>;
  @Output() selectedItem = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  clickItem(item: any) {
    this.selectedItem.emit(item);
  }

}
