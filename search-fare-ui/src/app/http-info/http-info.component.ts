import { Component, OnInit } from '@angular/core';

import { HttpInfoService } from '../shared/Service/http-info.service';
import { IHttpInfoModel } from '../shared/model/HttpInfo.model';

@Component({
  selector: 'app-http-info',
  templateUrl: './http-info.component.html',
  styleUrls: ['./http-info.component.scss']
})

export class HttpInfoComponent implements OnInit {

  public httpDetails;

  ngOnInit(): void {
    this.initGetHttpDetails();
  }

  constructor(private httpInfoService: HttpInfoService) { }

  initGetHttpDetails() {
    this.httpDetails = this.httpInfoService.getHttpDetails();
    this.httpDetails.response.subscribe((res: IHttpInfoModel) => {
      if (res) {
        this.httpDetails = res;
      }
    });
    this.httpDetails.get();
  }
}
